from submission.convert_to_binary import convert_to_binary


def test_convert_1() -> None:
    assert convert_to_binary(1) == "1"
