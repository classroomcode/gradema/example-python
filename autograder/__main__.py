import sys
from pathlib import Path

from gradema.grader.console import run_grader
from gradema.section import Section
from gradema.test import (
    create_python_format_check,
    create_python_type_check,
    create_python_traditional_stdio_test,
    create_file_exists_test,
    create_python_traditional_arg_test,
    create_python_pytest,
)
from gradema.test.argument import OUTPUT_FILE


def main(args: list[str]) -> int:
    print("Welcome to the autograder that uses gradema")
    test_directory = Path("autograder/test/")
    section = Section.pointed(
        100,
        "Binary Convert Programming Assignment",
        [
            Section.evenly_weighted(
                "Unit Tests",
                [
                    Section.evenly_weighted("Convert 0", create_python_pytest("autograder/test/unit/test_convert_0.py::test_convert_0")),
                    Section.evenly_weighted("Convert 1", create_python_pytest("autograder/test/unit/test_convert_1.py::test_convert_1")),
                    Section.evenly_weighted("Convert 5", create_python_pytest("autograder/test/unit/test_convert_5.py::test_convert_5")),
                ],
            ),
            Section.evenly_weighted(
                "Standard Input/Output Tests",
                [
                    Section.evenly_weighted(
                        "Convert 7 Stdio",
                        create_python_traditional_stdio_test(
                            "submission",
                            "convert_7",
                            test_directory / "stdio/inputs/decimal_7.txt",
                            test_directory / "stdio/goals/binary_7.txt",
                        ),
                    ),
                    Section.evenly_weighted(
                        "Convert 13 Stdio",
                        create_python_traditional_stdio_test(
                            "submission",
                            "convert_13",
                            test_directory / "stdio/inputs/decimal_13.txt",
                            test_directory / "stdio/goals/binary_13.txt",
                        ),
                    ),
                ],
            ),
            Section.evenly_weighted(
                "Args Tests",
                [
                    Section.evenly_weighted(
                        "Convert 19 Arg",
                        create_python_traditional_arg_test(
                            "submission",
                            "convert_19_arg",
                            [str(test_directory / "stdio/inputs/decimal_19.txt"), OUTPUT_FILE],
                            test_directory / "stdio/goals/binary_19.txt",
                        ),
                    ),
                ],
            ),
            Section.pointed(
                10,
                "Other Checks",
                [
                    Section.evenly_weighted("Formatting", create_python_format_check()),
                    Section.evenly_weighted("Type Checking", create_python_type_check()),
                ],
            ),
        ],
    )
    return run_grader(args, section)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
