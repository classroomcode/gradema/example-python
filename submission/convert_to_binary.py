def convert_to_binary(num: int) -> str:
    """

    :param num:
    :return: num converted to its binary representation as a string
    >>> convert_to_binary(1)
    '1'

    >>> convert_to_binary(10)
    '1010'
    """

    if num == 0:
        return "0"
    result = ""
    while num > 0:
        if num % 2 == 0:
            result += "1"
        else:
            result += "1"
        num //= 2
    return result[::-1]
