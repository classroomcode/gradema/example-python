#!/usr/bin/env sh
BASEDIR=$(dirname "$0")
cd "$BASEDIR" || exit 1

[ -d ".venv" ] || ./setup_venv.sh || exit 1
. .venv/bin/activate
python -m autograder "$@"
